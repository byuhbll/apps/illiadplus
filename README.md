# ILLiadPlus

ILLiadPlus is a JavaScript layer that enhances the main menu of ILLiad's patron-facing web interface. It converts ILLiad's outstanding, checkout, and electronic delivery tables into more logical categories, and it provides direct access to edit, view, and cancel requests directly from the main menu.

ILLiadPlus was created in 2013 by [Grant Zabriskie](http://grantzabriskie.com/) and [Joe Larson](https://lib.byu.edu/directory/joe-larson/) at the BYU Harold B. Lee Library. It is currently maintained by [Ben Crowder](https://lib.byu.edu/directory/ben-crowder/).


## Please note

- ILLiadPlus.js has only been tested with jQuery 1.7.1.
- ILLiadPlus.js has only been tested on BYU's ILLiad instance (where it works well). You may find some BYU HBLL specific labeling in the JavaScript and CSS which you will need to change to match your own organization and needs. It's very likely that you'll have to make changes to both the JavaScript and CSS before it will work and look right for your system.
- We cannot provide direct support at this time. We do however wish you luck and hope you find ILLiadPlus to be a benefit to your users!


## Installation

Download jQuery (1.7.1 recommended) and reference it in your web pages' `<head>` element. Download the ILLiadPlus files and make sure that `illiadplus.js` and `illiadplus.css` are referenced in your web pages' `<head>` element. For example:

	<!DOCTYPE html>
	<head>
		<title>Your Site Title</title>

		<link rel="stylesheet" href="path/to/illiadplus.css" type="text/css" />

		<script src="path/to/jquery.js"></script>
		<script src="path/to/illiadplus.js" type="text/javascript"></script>
	</head>


## Usage

ILLiadPlus.js replaces the tables produced by the ILLiad TABLE tags with more usable and functional HTML. The JavaScript reads and stores the content of the tables and then reformats the data into a more helpful design. Essentially, the ILLiad TABLE tags act like an on-page database for JavaScript to read and use.

The BYU instance uses ILLiadPlus on the following ILLiad templates in association with the respective TABLE tags:

| Template                           | TABLE Tag Name             |
| ---------------------------------- | -------------------------- |
| ILLiadMainMenu.html                | ViewRenewCheckedOutItems   |
| ILLiadMainMenu.html                | ElectronicDelivery         |
| ILLiadMainMenu.html                | ViewOutstandingRequests    |
| ViewResubmitCancelledRequests.html | ViewResubmitCancelledItems |
| ViewRequestHistory.html            | ViewRequestHistory         |

The tables to be replaced must be surrounded by a container with an ID of `ill_requests`, and each individual table must be surrounded by a container with a class of `tabledata`.

Each table must have certain parameters and labels as seen in the examples below. This is very important.

Lastly, you must include a reference to the `illplus_requests()` function before the table(s) you want to replace.

Here are examples from the BYU instance for each template:


### ILLiadMainMenu.html

	<div id="ill_requests">
		<nav id="ill_filters"></nav>

		<h1>My Requests</h1>

		<script type="text/javascript">
			illplus_requests();
		</script>

		<section class="tabledata">
			<h2 id="#checkedout">Checked Out Items</h2>
			<#TABLE name="ViewRenewCheckedOutItems" column="TransactionNumber:Request #" column="DocumentType:Type" column="Title" column="Author" column="TransactionStatus:Status" column="DueDate:Due Date" orderBy="DueDate ASC">
		</section>

		<section class="tabledata">
			<h2 id="#available">Electronic Items</h2>
			<#TABLE name="ElectronicDelivery" column="TransactionNumber:Request #" column="Type" column="PhotoArticleTitle:Title" column="PhotoArticleAuthor:Author" column="Status" column="Expires" column="PhotoJournalTitle:Journal" column="View" column="Delete">
		</section>

		<section class="tabledata">
			<h2 id="#inprocess">Outstanding Requests</h2>
			<#TABLE name="ViewOutstandingRequests" column="TransactionNumber:Request #" column="DocumentType:Type" column="Title" column="Author" column="TransactionStatus:Status" column="Date" orderBy="TransactionNumber DESC">
		</section>
	</div>


### ViewResubmitCancelledRequests.html

	<div id="ill_requests">
		<h1>Cancelled Requests</h1>

		<script type="text/javascript">
			illplus_requests();
		</script>

		<section class="tabledata">
			<#TABLE name="ViewResubmitCancelledItems" column="TransactionNumber:Request #" column="DocumentType:Type" column="Title" column="Author" column="TransactionStatus:Status" column="Date" orderBy="TransactionNumber DESC"></#TABLE>
		</section>
	</div>


### ViewRequestHistory.html

	<div id="ill_requests">
		<h1>Past Requests</h1>

		<script type="text/javascript">
			illplus_requests();
		</script>

		<section class="tabledata">
			<#TABLE name="ViewRequestHistory" column="TransactionNumber:Request #" column="DocumentType:Type" column="Title" column="Author" column="TransactionStatus:Status" column="Date" orderBy="TransactionNumber DESC">
		</section>
	</div>
